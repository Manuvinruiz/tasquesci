*****************************************
Benvinguts a la pàgina de tasques de CI
*****************************************

En aquest document trobareu els exercicis que s'han de fer a la sessió de laboratori de CI.


Sessió 5 :GLCD
######################################

L'exercici 1 és obligatori per aprovar la pràctica. Teniu dos opcions com a treball a classe. La primera opció té un nivell de dificultad menor que la segona. Amb la segona opció teniu la possibilitat d'arribar a 10, amb la primera a 8.5. Tot depenent de l'implementació feta, es clar.

Recordeu que teniu de support el sistema de debug del Proteus i el manual del compilador XC8 per utilitzar les funcions que calguin.   
Quan acabeu cadascun dels exercicis, aviseu al professor. 



Exercici 1 (5/10 punts)
************************
Mostrar els exercicis previs funcionant a la placa, polsant el botons segons s'indica a continuació:

* Botó d'entrada RA0: Mostrar imatge.
* Botó d'entrada RA1: Mostrar informació del grup "amb" la data d'avuí (8-04-2016).
* Botó d'entrada RA2: Mostrar el punt que es mou per tota la pantalla (tots els pixels 64x128).


Recordeu de configurar els connectors la tarjeta Easypic per tenir el segúent esquema del botó, com ja heu fet en pràctiques anteriors.

.. figure:: buttons.png
   :height: 500px
   :width: 500 px
   :scale: 100%
   :align: center

Eviteu els rebots!. A continuació es mostra una imatge amb pistes de les zones que heu de configurar a la placa per a que tot funcioni.

.. figure:: placa.png
   :height: 500px
   :width: 500 px
   :scale: 100%
   :align: center


OPCIÓ A: CONFIGURACIÓ DE TEXT 
*****************************
En aquesta opció trobareu diferents exercicis relacionats amb noves opcions de text. 
Aquesta opció permet arribar a una nota màxima de 8.5 segons l'implementació feta.


Exercici A.1
---------------
Feu una funció que centri el text en una de les pàgines del LCD. 
Aquest exercici té una nota màxima de 1 punt. 

Exercici A.2
---------------
Feu una funció que faci aparèixer  un text des de la dreta del LCD lletra a lletra en direcció a l'esquerra, tot donat una pàgina del LCD.S'haurà d'aturar quan el primer caràcter del text arribi a la posició 0 en Y de la pàgina. 
Aquest exercici té una nota màxima de 1 punt. 

Exercici A.3
--------------
Feu una funció com la d'abans, però que també desaparegui per l'esquerra quan arribin els primers caràcters. 
Aquest exercici té una nota màxima de 0.75 punt. 

Exercici A.4
--------------
Feu una funció que faci el subrallat d'un text d'entrada utilitzant la funció setDot.
Aquest exercici té una nota màxima de 0.75 punt. 
 
 
OPCIÓ B: VIDEJOC 
******************************
En aquesta opció trobareu diferents exercicis que permeten arribar a una primera etapa del joc del PACMAN. 
Aquest exercici permet arribar a una nota màxima de 10 segons l'implementació feta. 

Exercici B.1 
--------------
Afegiu els següents codis Ascii a la taula font5x7 situada en "ascii.h":


.. figure:: newchars.png
   :height: 500px
   :width: 500 px
   :scale: 100%
   :align: center

A continuació, visualitzeu aquest caràcters a la pantalla de forma aleatòria i guardeu els valors de les seves posicions X i Y.  
Aquest exercici afegeix un màxim de 2 punts a la nota.

Exercici B.2
-------------

Afegiu la funcionalitat de poder moure uns del caràcters anteriors per la pantalla segons preneu els botons del port A, tal i com s'indica a la següent imatge.

.. figure:: moveChar.png
   :height: 500px
   :width: 500 px
   :scale: 100%
   :align: center
   
Teniu tres possibilitats a escogir:

* Moure el caràcter en X per pàgina i en Y per pixel (1/3 de la nota del exercici).
* Moure el caràcter a "salts" de pàgina tant en X com en Y. (2/3 de la nota).
* Moure el caràcter pixel a pixel en ambdós direccions. (3/3 de la nota). 

Per a la tercera opció de moviment, podeu pensar en fer el següent:

* Funció onPixel(int cx, int cy, char c) que mira si s'ha de pintar o esborrar un pixel dins del caràcter (matriu 5x7).
* Funció onChar(int x,int y, char c) que utilitza la funció anterior per fer una cerca a la matriu 5x7 del caràcter per pintar o no un pixel de la pantalla, relatiu a la posició x,y d'entrada que és una posició dins del GLCD (64x128) on es troba actualment el caràcter. Podeu usar la funció setDot i clearDot per pintar o esborrar el píxel en concret.

Aquest exercici afegeix un màxim de 2 punts a la nota.

 
Exercici B.3
-------------
Feu que quan s'acosti el caràcter que es mou a l'altre que no es mou, es vegi l'efecte d'una explosió. Aquest efecte podría ser fer aparèixer el caràcter "*" per exemple. 

Teniu dos possibilitats:

* Calcular la col.lisió per pàgina. Aquest cas es dona si heu implementat alguna de les dues opcions primeres de l'exercici anteriror. (1/2 de la nota)
* Calcular si algun dels píxels de les quatre cantonades del caràcter en moviment està "dins" de l'espai ocupat per l'altre caràcter estàtic. Això és fàcil si mireu que les coordenades dels punts estàn inclosses dins del rang de les dues cantonades en X, o de les dues cantonades en Y.(2/2 de la nota).

Aquest exercici afegeix un valor màxim de 1 punt a la nota. 






 


